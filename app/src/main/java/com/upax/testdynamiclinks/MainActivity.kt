package com.upax.testdynamiclinks

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.upax.captain.CaptainActivity
import com.upax.spider.SpiderActivity
import com.upax.thor.ThorActivity

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getDynamicLink()

        findViewById<Button>(R.id.btn_module_spider).setOnClickListener {
            startActivity(Intent(this, SpiderActivity::class.java))
        }

        findViewById<Button>(R.id.btn_module_thor).setOnClickListener {
            startActivity(Intent(this, ThorActivity::class.java))
        }

    }

    fun getDynamicLink() {

        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(this) {
                val deepLink: Uri
                if (it != null) {
                    deepLink = it.link!!
                    Log.e(TAG, "getDynamicLink:deepLink ${deepLink.toString()}")
                    val encodedPath = deepLink.encodedPath
                    if (encodedPath != null) {
                        if (encodedPath.contains("thor")) {
                            startActivity(Intent(this, ThorActivity::class.java))
                            finish()
                        } else if (encodedPath.contains("test")) {
                            startActivity(Intent(this, CaptainActivity::class.java))
                        }
                    }

                } else {
                    Log.e(TAG, "getDynamicLink:deepLink null")
                }
            }
            .addOnFailureListener(this) { e ->
                Log.e(TAG, "getDynamicLink:onFailure", e)
            }

    }

}